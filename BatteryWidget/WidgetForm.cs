﻿using System.Drawing;
using System.Windows.Forms;
using BatteryWidget.Properties;

namespace BatteryWidget {

    public partial class WidgetForm : Form {

        private const int HideControlsInterval = 1000;
        private const int UpdateInfoInterval = 60 * 1000;

        /** Contol moving window */
        private bool _windowDragStatus;
        private Point _clickPoint;

        private Timer _updateInfoTimer;
        private Timer _hideControlsTimer;

        private readonly Image _grayImage;

        private readonly BatteryGraph _graph;

        
        #region Initialize
        public WidgetForm() {
            InitializeComponent();

            Config.ReadConfig();
            titleLabel.Text = Config.Title;
            this.Text = Config.Title;
            this.Size = Config.WindowSize;

            imageLabel.Image = Config.Get(Config.Name.Green);
            _grayImage = Config.Get(Config.Name.Gray);

            _graph = new BatteryGraph();

            InitTimers();
            InitWindow();
            initMouseEventHandler();

            UpdateInfo();
        }

        private void InitTimers() {
            // Timer for hide window's title
            _hideControlsTimer = new Timer();
            _hideControlsTimer.Interval = HideControlsInterval;
            _hideControlsTimer.Tick += (s1, e1) => {
                _hideControlsTimer.Stop();
                titleLabel.Hide();
                exitLabel.Hide();
            };

            // Timer for update info
            _updateInfoTimer = new Timer();
            _updateInfoTimer.Interval = UpdateInfoInterval;
            _updateInfoTimer.Tick += (s, e) => UpdateInfo();
            _updateInfoTimer.Start();
        }
        
        private void InitWindow() {
            this.TransparencyKey = this.BackColor;
            this.ShowInTaskbar = false;

            // Hide controls
            titleLabel.Hide();
            exitLabel.Hide();

            // Restore window position
            this.Location = Settings.Default.windowPosition;
        }

        private void initMouseEventHandler() {
            // Moving window by title
            titleLabel.MouseDown += (s, e) => {
                if (e.Button == MouseButtons.Left) {
                    // Moving mode
                    _windowDragStatus = true;
                    _clickPoint = new Point(e.X, e.Y);
                } else {
                    _windowDragStatus = false;
                }
            };
            titleLabel.MouseMove += (s, e) => {
                if (_windowDragStatus) {
                    Point pointMoveTo = this.PointToScreen(new Point(e.X, e.Y));
                    pointMoveTo.Offset(-_clickPoint.X, -_clickPoint.Y);
                    this.Location = pointMoveTo;
                }
            };
            titleLabel.MouseUp += (s, e) => {
                _windowDragStatus = false;
                Settings.Default.windowPosition = this.Location;
                Settings.Default.Save();
            };

            // Exit button
            exitLabel.MouseClick += (s, e) => this.Close();

            // Show/hide title and exit button
            imageLabel.MouseEnter += (s, e) => {
                // Show controls
                titleLabel.Show();
                exitLabel.Show();
                _hideControlsTimer.Stop();
            };
            imageLabel.MouseLeave += (s, e) => {
                // Hide controls
                _hideControlsTimer.Start();
            };
            imageLabel.MouseClick += (s, e) => {
                // Change view by click: graph/image
                _graph.RevertShowState();
                UpdateInfo();
            };
        } 
        #endregion

        private void UpdateInfo() {
            int percent = Battery.GetBatteryPercentage();
            _graph.AddPercent(percent);

            // Display graph
            if (_graph.GraphIsShowing()) {
                Bitmap bmp = new Bitmap(Size.Width, Size.Height);
                Graphics g = Graphics.FromImage(bmp);
                _graph.DrawGraph(g);
                g.Dispose();

                imageLabel.Text = "";
                imageLabel.Image = bmp;
                return;
            }

            imageLabel.Text = percent + "%";
            imageLabel.Image = MakeImage(percent);
        }

        protected override CreateParams CreateParams {
            get {
                var cp = base.CreateParams;
                cp.ExStyle |= 0x80;  // Turn on WS_EX_TOOLWINDOW
                return cp;
            }
        }

        #region Images
        /// <summary>
        /// Gets an image by battery percentage.
        /// </summary>
        /// <param name="percent"></param>
        /// <returns></returns>
        private Image MakeImage(int percent) {
            if (percent <= 8) {
                return WarningStateImage();
            }
            
            _updateInfoTimer.Interval = UpdateInfoInterval;
            
            if (Battery.IsBatteryPowerOn()) {
                return PowerStateImage();
            }

            return NormalStateImage(percent);
        }

        private Image PowerStateImage() {
            imageLabel.BackColor = Config.PowerStateColor;
            return Config.Get(Config.Name.Power);
        }

        private Image NormalStateImage(int percent) {
            imageLabel.BackColor = Config.NormalStateColor;

            Bitmap bmp = new Bitmap(SelectImage(percent));

            int y = (100 - percent) * bmp.Height / 100;
            Rectangle destRect = new Rectangle(0, 0, bmp.Width, y);

            Graphics g = Graphics.FromImage(bmp);
            g.DrawImageUnscaledAndClipped(_grayImage, destRect);
            g.Dispose();

            return bmp;
        }

        private Image WarningStateImage() {
            _updateInfoTimer.Interval = 800;
            if (imageLabel.Image.Equals(_grayImage)) {
                imageLabel.BackColor = Config.NormalStateColor;
                return Config.Get(Config.Name.Red);
            }
            imageLabel.BackColor = Config.WarningStateColor;
            return _grayImage;
        }

        /// <summary>
        /// Select an image by battery percentage in normal state.
        /// </summary>
        private static Image SelectImage(int percent) {
            if (percent < 30)
                return Config.Get(Config.Name.Red);
            if (percent < 70)
                return Config.Get(Config.Name.Yellow);
            return Config.Get(Config.Name.Green);
        } 
        #endregion
    }
}