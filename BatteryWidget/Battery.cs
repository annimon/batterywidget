﻿using System.Windows.Forms;

namespace BatteryWidget {

    public class Battery {

        public static int GetBatteryPercentage() {
            return (int) (SystemInformation.PowerStatus.BatteryLifePercent * 100);
        }

        public static bool IsBatteryPowerOn() {
            return (SystemInformation.PowerStatus.PowerLineStatus == PowerLineStatus.Online);
        }
    }
}