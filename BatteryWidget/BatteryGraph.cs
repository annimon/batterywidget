﻿using System.Collections.Generic;
using System.Drawing;

namespace BatteryWidget {

    public class BatteryGraph {

        private const int MaxGraphPoints = 50;

        private bool _isShowing;
        private readonly List<int> _percentage;

        public BatteryGraph() {
            _isShowing = false;

            _percentage = new List<int>();
        }

        public bool GraphIsShowing() {
            return _isShowing;
        }

        public bool RevertShowState() {
            _isShowing = !_isShowing;
            return _isShowing;
        }

        public void AddPercent(int currentPercentage) {
            _percentage.Add(currentPercentage);

            while (_percentage.Count >= MaxGraphPoints) {
                _percentage.RemoveAt(0);
            }
        }

        public void DrawGraph(Graphics g) {
            float width = g.VisibleClipBounds.Width;
            float height = g.VisibleClipBounds.Height;

            float deltaX = width / MaxGraphPoints;
            float x1 = 0;
            float y1 = (100 - _percentage[0]) * height / 100;
            for (int i = 1; i < _percentage.Count; i++) {
                float x2 = x1 + deltaX;
                float y2 = (100 - _percentage[i]) * height / 100;

                g.DrawLine(Pens.Silver, x1, y1, x2, y2);

                x1 = x2;
                y1 = y2;
            }
        }
    }
}