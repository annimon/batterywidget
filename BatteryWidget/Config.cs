﻿using System;
using System.Drawing;
using System.IO;
using System.Resources;
using BatteryWidget.Properties;

namespace BatteryWidget {

    public static class Config {

        private const string Path = "res";
        private const string ConfigFile = "config";

        /// <summary>
        /// Image state names.
        /// </summary>
        public enum Name {
            Gray,
            Red,
            Yellow,
            Green,
            Power
        }

        public static string Title;
        public static Size WindowSize;
        public static Color WarningStateColor, NormalStateColor, PowerStateColor;

        private static readonly Image[] Cache = new Image[(int) Name.Power + 1];

        /// <summary>
        /// Get image from cache or filesystem (with store to cache).
        /// </summary>
        /// <param name="name">image state name</param>
        /// <returns>Image object</returns>
        public static Image Get(Name name) {
            int index = (int) name;

            // Check cache availability
            if (Cache[index] != null) return Cache[index];

            // Try to load image from file
            string resName = name.ToString().ToLower();
            Image img;
            try {
                img = LoadImage(resName);
            } catch (Exception) {
                ResourceManager rm = Resources.ResourceManager;
                img = (Image) rm.GetObject(resName);
            }

            Cache[index] = img;
            return img;
        }

        /// <summary>
        /// Read config with colors, names etc.
        /// </summary>
        public static void ReadConfig() {
            try {
                using (var reader = new StreamReader(Path + System.IO.Path.DirectorySeparatorChar + ConfigFile)) {
                    // Title
                    Title = ReadLine(reader).Trim();
                    // Size
                    WindowSize = ReadSize(reader);
                    // Colors
                    WarningStateColor = ReadColor(reader);
                    NormalStateColor = ReadColor(reader);
                    PowerStateColor = ReadColor(reader);
                }
            } catch (Exception) {
                // Default configuration
                Title = "Android Battery Widget";
                WindowSize = new Size(120, 69);
                WarningStateColor = Color.Red;
                NormalStateColor = Color.DarkGreen;
                PowerStateColor = Color.Black;
            }
        }


        /// <summary>
        /// Read image from filesystem.
        /// </summary>
        private static Image LoadImage(string name) {
            string filename = Path + System.IO.Path.DirectorySeparatorChar + name + ".png";
            using (var fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read)) {
                return new Bitmap(fs);
            }
        }

        /// <summary>
        /// Read color from stream.
        /// </summary>
        private static Color ReadColor(StreamReader reader) {
            string line = ReadLine(reader);
            return ColorTranslator.FromHtml(line);
        }

        /// <summary>
        /// Read size value from stream.
        /// </summary>
        private static Size ReadSize(StreamReader reader) {
            string line = ReadLine(reader);
            string[] arr = line.Split(';');
            int width = Convert.ToInt16(arr[0]);
            int height = Convert.ToInt16(arr[1]);
            return new Size(width, height);
        }

        /// <summary>
        /// Read line missing comments.
        /// </summary>
        private static string ReadLine(StreamReader reader) {
            string line;
            do {
                line = reader.ReadLine();
            } while ((line != null) && ( (line.Trim().Length == 0) || (line.StartsWith("//")) ));
            return line;
        }
    }
}
